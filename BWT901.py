import serial

# TODO /dev/ttyXXX is connection specific
port = serial.Serial("/dev/ttyUSB0", baudrate=115200, timeout=1.0)  
# TODO byteorder (big or little) need to be corrected
endian = 'little' 

while True:
    firstbyte = port.read()                         #read 1 byte
    if firstbyte == bytes.fromhex('55'):            # start of data package
        secondbyte = port.read()                    #read next byte
        if secondbyte == bytes.fromhex('51'):       # start of acceleration data
            accel = []
            for i in range(3):                      #read next 3x2bytes and convert to interger
                accel.append(int.from_bytes(port.read(2), byteorder=endian, signed=True)/32768.0*16)
            temp = int.from_bytes(port.read(2), byteorder=endian, signed=True)/340.0+36.25
            print("Acceleration:")
            print(accel)
        elif secondbyte == bytes.fromhex('52'):     #start of angle velocity data
            angVel = []
            for i in range(3):
                angVel.append(int.from_bytes(port.read(2), byteorder=endian, signed=True)/32768.0*2000)
            temp = int.from_bytes(port.read(2), byteorder=endian, signed=True)/340.0+36.25
            print("Angular Velocity:")
            print(angVel)
        elif secondbyte == bytes.fromhex('53'):     #start of angle data
            angle = []
            for i in range(3):
                angle.append(int.from_bytes(port.read(2), byteorder=endian, signed=True)/32768.0*180)
            temp = int.from_bytes(port.read(2), byteorder=endian, signed=True)/340.0+36.25
            print("Angle:")
            print(angle)

        