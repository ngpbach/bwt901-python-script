Super simple python script to retrieve data from BWT901 imu module through serial port (such as the cp210x usb-serial converter).

Tested working with bluetooth serial (/dev/rfcomm0)

The device in the script need to be modified to the correct com port. In linux, immediately after connecting the usb, run:

	dmesg

... the last few lines will tell which tty it was attached to (example: pl2303 converter now attached to ttyUSB0), then the path would be "/dev/ttyUSB0"

In windows open device manager to find out the com port number (example: 'COM4')

Run the following commands in linux:

	sudo apt update

	sudo apt upgrade

	sudo apt install python3 python3-serial

	sudo adduser $USER dialout

To run the script:

	python3 ./BWT901.py

To find out list of available ports:

	python3 -m serial.tools.list_ports